scikit-image>=0.15
elasticsearch<7,>=6.4.0

[dev]
ipdb
ipython
coverage
pep8
pyflakes
pylint
pytest
pytest-cov
pytest-xdist
recommonmark>=0.4.0
Sphinx>=1.3.5
sphinxcontrib-napoleon>=0.4.4
sphinx-rtd-theme>=0.1.9

[docs]
recommonmark>=0.4.0
Sphinx>=1.3.5
sphinxcontrib-napoleon>=0.4.4
sphinx-rtd-theme>=0.1.9

[extra]
cairosvg<2,>1

[test]
coverage
pep8
pyflakes
pylint
pytest
pytest-cov
pytest-xdist
